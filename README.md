gedit
=====

gedit is an easy-to-use and general-purpose text editor. Its development started
in 1998, at the beginnings of the GNOME project, with a good integration with
that desktop environment.

You can use it to write simple notes and documents, or you can enable more
advanced features that are useful for software development.

See [gedit-text-editor.org](https://gedit-text-editor.org/) for more
information.

License
-------

gedit is released under the GNU General Public License (GPL) version 2 or later,
see the [COPYING](COPYING) file for more information.

Dependencies
------------

gedit depends on GTK 3 and
[Gedit Technology](https://gedit-text-editor.org/technology.html).

For a complete list of dependencies, see the [meson.build](meson.build) file.

Installation
------------

To build gedit from source, see the [docs/build.md](docs/build.md) file.

How to report bugs
------------------

Please read
[the web page on how to report bugs](https://gedit-text-editor.org/reporting-bugs.html),
it contains a list of frequently reported bugs and a link to the bug
tracker.

Development resources
---------------------

See the [Gedit Technology](https://gedit-text-editor.org/technology.html) web
page.

Contributions
-------------

gedit development relies on voluntary contributions and everyone is invited to
help.

See the [CONTRIBUTING.md](CONTRIBUTING.md) file for more information.
