/* SPDX-FileCopyrightText: 2014 - Ignacio Casal Quinteiro
 * SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gedit-spell-app-activatable.h"
#include <glib/gi18n.h>
#include <gedit/gedit-app-activatable.h>
#include <gedit/gedit-app.h>

struct _GeditSpellAppActivatablePrivate
{
	GeditApp *app;
	GeditMenuExtension *menu_extension;
};

enum
{
	PROP_0,
	PROP_APP
};

static void gedit_app_activatable_iface_init (GeditAppActivatableInterface *iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED (GeditSpellAppActivatable,
				gedit_spell_app_activatable,
				G_TYPE_OBJECT,
				0,
				G_ADD_PRIVATE_DYNAMIC (GeditSpellAppActivatable)
				G_IMPLEMENT_INTERFACE_DYNAMIC (GEDIT_TYPE_APP_ACTIVATABLE,
							       gedit_app_activatable_iface_init))

static void
gedit_spell_app_activatable_get_property (GObject    *object,
                                          guint       prop_id,
                                          GValue     *value,
                                          GParamSpec *pspec)
{
	GeditSpellAppActivatable *activatable = GEDIT_SPELL_APP_ACTIVATABLE (object);

	switch (prop_id)
	{
		case PROP_APP:
			g_value_set_object (value, activatable->priv->app);
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void
gedit_spell_app_activatable_set_property (GObject      *object,
                                          guint         prop_id,
                                          const GValue *value,
                                          GParamSpec   *pspec)
{
	GeditSpellAppActivatable *activatable = GEDIT_SPELL_APP_ACTIVATABLE (object);

	switch (prop_id)
	{
		case PROP_APP:
			g_assert (activatable->priv->app == NULL);
			activatable->priv->app = GEDIT_APP (g_value_dup_object (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void
gedit_spell_app_activatable_dispose (GObject *object)
{
	GeditSpellAppActivatable *activatable = GEDIT_SPELL_APP_ACTIVATABLE (object);

	g_clear_object (&activatable->priv->app);
	g_clear_object (&activatable->priv->menu_extension);

	G_OBJECT_CLASS (gedit_spell_app_activatable_parent_class)->dispose (object);
}

static void
gedit_spell_app_activatable_class_init (GeditSpellAppActivatableClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->get_property = gedit_spell_app_activatable_get_property;
	object_class->set_property = gedit_spell_app_activatable_set_property;
	object_class->dispose = gedit_spell_app_activatable_dispose;

	g_object_class_override_property (object_class, PROP_APP, "app");
}

static void
gedit_spell_app_activatable_class_finalize (GeditSpellAppActivatableClass *klass)
{
}

static void
gedit_spell_app_activatable_init (GeditSpellAppActivatable *activatable)
{
	activatable->priv = gedit_spell_app_activatable_get_instance_private (activatable);
}

static void
add_accels (GtkApplication *app)
{
	const gchar *accels[] = { "<Shift>F7", NULL };

	gtk_application_set_accels_for_action (app, "win.check-spell", accels);
}

static void
remove_accels (GtkApplication *app)
{
	const gchar *accels[] = { NULL };

	gtk_application_set_accels_for_action (app, "win.check-spell", accels);
}

static void
extend_menu (GeditSpellAppActivatable *activatable)
{
	GMenuItem *item;

	g_clear_object (&activatable->priv->menu_extension);
	activatable->priv->menu_extension = gedit_app_activatable_extend_menu (GEDIT_APP_ACTIVATABLE (activatable),
									       "spell-section");

	item = g_menu_item_new (_("_Check Spelling…"), "win.check-spell");
	gedit_menu_extension_append_menu_item (activatable->priv->menu_extension, item);
	g_object_unref (item);

	item = g_menu_item_new (_("Set _Language…"), "win.config-spell");
	gedit_menu_extension_append_menu_item (activatable->priv->menu_extension, item);
	g_object_unref (item);

	item = g_menu_item_new (_("_Highlight Misspelled Words"), "win.inline-spell-checker");
	gedit_menu_extension_append_menu_item (activatable->priv->menu_extension, item);
	g_object_unref (item);
}

static void
gedit_spell_app_activatable_activate (GeditAppActivatable *app_activatable)
{
	GeditSpellAppActivatable *activatable = GEDIT_SPELL_APP_ACTIVATABLE (app_activatable);

	add_accels (GTK_APPLICATION (activatable->priv->app));
	extend_menu (activatable);
}

static void
gedit_spell_app_activatable_deactivate (GeditAppActivatable *app_activatable)
{
	GeditSpellAppActivatable *activatable = GEDIT_SPELL_APP_ACTIVATABLE (app_activatable);

	remove_accels (GTK_APPLICATION (activatable->priv->app));
	g_clear_object (&activatable->priv->menu_extension);
}

static void
gedit_app_activatable_iface_init (GeditAppActivatableInterface *iface)
{
	iface->activate = gedit_spell_app_activatable_activate;
	iface->deactivate = gedit_spell_app_activatable_deactivate;
}

void
gedit_spell_app_activatable_register (PeasObjectModule *module)
{
	gedit_spell_app_activatable_register_type (G_TYPE_MODULE (module));

	peas_object_module_register_extension_type (module,
						    GEDIT_TYPE_APP_ACTIVATABLE,
						    GEDIT_TYPE_SPELL_APP_ACTIVATABLE);
}
