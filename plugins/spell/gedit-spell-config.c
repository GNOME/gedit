/* SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gedit-spell-config.h"
#include <glib/gi18n.h>
#include <libpeas-gtk/peas-gtk.h>
#include <gspell/gspell.h>
#include <tepl/tepl.h>

struct _GeditSpellConfigPrivate
{
	GSettings *settings;
};

static void peas_gtk_configurable_iface_init (PeasGtkConfigurableInterface *iface);

G_DEFINE_DYNAMIC_TYPE_EXTENDED (GeditSpellConfig,
				gedit_spell_config,
				G_TYPE_OBJECT,
				0,
				G_ADD_PRIVATE_DYNAMIC (GeditSpellConfig)
				G_IMPLEMENT_INTERFACE_DYNAMIC (PEAS_GTK_TYPE_CONFIGURABLE,
							       peas_gtk_configurable_iface_init))

static void
gedit_spell_config_dispose (GObject *object)
{
	GeditSpellConfig *self = GEDIT_SPELL_CONFIG (object);

	g_clear_object (&self->priv->settings);

	G_OBJECT_CLASS (gedit_spell_config_parent_class)->dispose (object);
}

static void
gedit_spell_config_class_init (GeditSpellConfigClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->dispose = gedit_spell_config_dispose;
}

static void
gedit_spell_config_class_finalize (GeditSpellConfigClass *klass)
{
}

static void
gedit_spell_config_init (GeditSpellConfig *self)
{
	self->priv = gedit_spell_config_get_instance_private (self);
	self->priv->settings = g_settings_new ("org.gnome.gedit.plugins.spell");
}

static GtkWidget *
create_language_chooser (GeditSpellConfig *self)
{
	GtkWidget *hgrid;
	GtkWidget *language_chooser_button;

	hgrid = gtk_grid_new ();
	gtk_grid_set_column_spacing (GTK_GRID (hgrid), 6);

	gtk_container_add (GTK_CONTAINER (hgrid),
			   /* Translators: For a natural language. */
			   gtk_label_new_with_mnemonic (_("_Language:")));

	language_chooser_button = gspell_language_chooser_button_new (NULL);

	g_settings_bind (self->priv->settings, "spell-checking-language",
			 language_chooser_button, "language-code",
			 G_SETTINGS_BIND_DEFAULT);

	gtk_container_add (GTK_CONTAINER (hgrid), language_chooser_button);

	return hgrid;
}

static GtkWidget *
create_highlight_misspelled_words_checkbutton (GeditSpellConfig *self)
{
	return tepl_prefs_create_checkbutton_simple (self->priv->settings,
						     "highlight-misspelled-words",
						     _("_Highlight misspelled words"));
}

static GtkWidget *
gedit_spell_config_create_configure_widget (PeasGtkConfigurable *configurable)
{
	GeditSpellConfig *self = GEDIT_SPELL_CONFIG (configurable);
	GtkWidget *vgrid;
	GtkWidget *component;

	vgrid = gtk_grid_new ();
	gtk_orientable_set_orientation (GTK_ORIENTABLE (vgrid), GTK_ORIENTATION_VERTICAL);
	gtk_grid_set_row_spacing (GTK_GRID (vgrid), 6);

	gtk_container_add (GTK_CONTAINER (vgrid),
			   create_language_chooser (self));

	gtk_container_add (GTK_CONTAINER (vgrid),
			   create_highlight_misspelled_words_checkbutton (self));

	component = tepl_utils_get_titled_component (_("Default Settings"), vgrid);
	g_object_set (component,
		      "margin", 12,
		      NULL);

	gtk_widget_show_all (component);
	return component;
}

static void
peas_gtk_configurable_iface_init (PeasGtkConfigurableInterface *iface)
{
	iface->create_configure_widget = gedit_spell_config_create_configure_widget;
}

void
gedit_spell_config_register (PeasObjectModule *module)
{
	gedit_spell_config_register_type (G_TYPE_MODULE (module));

	peas_object_module_register_extension_type (module,
						    PEAS_GTK_TYPE_CONFIGURABLE,
						    GEDIT_TYPE_SPELL_CONFIG);
}
