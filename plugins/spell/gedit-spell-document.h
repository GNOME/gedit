/* SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gspell/gspell.h>
#include <gedit/gedit-document.h>

G_BEGIN_DECLS

GspellChecker *	gedit_spell_document_get_spell_checker				(GeditDocument *document);

void		gedit_spell_document_set_metadata_for_language			(GeditDocument        *document,
										 const GspellLanguage *language);

void		gedit_spell_document_set_metadata_for_inline_spell_checking	(GeditDocument *document,
										 gboolean       inline_spell_checking);

void		gedit_spell_document_get_metadata_for_inline_spell_checking	(GeditDocument *document,
										 gboolean      *metadata_exists,
										 gboolean      *metadata_value);

void		gedit_spell_document_setup_spell_checker			(GeditDocument *document);

void		gedit_spell_document_restore_language				(GeditDocument *document);

G_END_DECLS
