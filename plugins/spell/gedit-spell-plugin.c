/* SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gedit-spell-plugin.h"
#include "gedit-spell-app-activatable.h"
#include "gedit-spell-config.h"
#include "gedit-spell-window-activatable.h"

G_MODULE_EXPORT void
peas_register_types (PeasObjectModule *module)
{
	gedit_spell_app_activatable_register (module);
	gedit_spell_window_activatable_register (module);
	gedit_spell_config_register (module);
}
