/* SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <libpeas/peas.h>

G_BEGIN_DECLS

#define GEDIT_TYPE_SPELL_CONFIG             (gedit_spell_config_get_type ())
#define GEDIT_SPELL_CONFIG(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GEDIT_TYPE_SPELL_CONFIG, GeditSpellConfig))
#define GEDIT_SPELL_CONFIG_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GEDIT_TYPE_SPELL_CONFIG, GeditSpellConfigClass))
#define GEDIT_IS_SPELL_CONFIG(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GEDIT_TYPE_SPELL_CONFIG))
#define GEDIT_IS_SPELL_CONFIG_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GEDIT_TYPE_SPELL_CONFIG))
#define GEDIT_SPELL_CONFIG_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GEDIT_TYPE_SPELL_CONFIG, GeditSpellConfigClass))

typedef struct _GeditSpellConfig         GeditSpellConfig;
typedef struct _GeditSpellConfigClass    GeditSpellConfigClass;
typedef struct _GeditSpellConfigPrivate  GeditSpellConfigPrivate;

struct _GeditSpellConfig
{
	GObject parent;

	GeditSpellConfigPrivate *priv;
};

struct _GeditSpellConfigClass
{
	GObjectClass parent_class;
};

GType	gedit_spell_config_get_type	(void) G_GNUC_CONST;

void	gedit_spell_config_register	(PeasObjectModule *module);

G_END_DECLS
