Third-party gedit plugins
=========================

Plugins that are neither part of gedit nor gedit-plugins. They can be installed
separately.

- [Control Your Tabs](https://github.com/jefferyto/gedit-control-your-tabs)
- [Ex-Mortis](https://github.com/jefferyto/gedit-ex-mortis)
- [Markdown Preview](https://github.com/maoschanz/gedit-plugin-markdown_preview)
- [Python Console](https://github.com/jefferyto/gedit-pythonconsole)
- [reStructuredText Preview](https://github.com/bittner/gedit-reST-plugin)
- [Tab Group Salute](https://github.com/jefferyto/gedit-tab-group-salute)
