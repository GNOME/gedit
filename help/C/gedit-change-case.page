<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="gedit-change-case">

  <info>
    <link type="guide" xref="index#gedit-working-with-files" group="seventh" />
    <desc>Change the case of selected text.</desc>
  </info>

  <title>Change case</title>

  <p>
    This feature helps you to change the case of selected portions of text. You
    can use it to change text to be all upper case, all lower case, to invert
    the case, or apply title case.
  </p>

  <p>You can change the case by completing the following steps:</p>

  <steps>
    <item><p>Highlight the portion of text that you want to change.</p></item>
    <item><p>Right-click on the selected text to open the context menu.</p></item>
    <item><p>
      Choose <gui>Change Case</gui> and then your desired text-formatting
      option.
    </p></item>
  </steps>

  <p>The updates to the text formatting will take place immediately.</p>

  <note style="tip">
    <p>
      The <gui>Invert Case</gui> option will convert all lower case letters to
      upper case, and will convert all upper case letters to lower case.
    </p>
    <p>
      The <gui>Title Case</gui> option will convert the first letter of each
      word to upper case. All other letters will be converted to lower case.
    </p>
  </note>

  <note>
    <p>
      If you have not highlighted any text, the <em>Change Case</em> feature
      will be grayed-out. You need to select a portion of text before you use
      the <em>Change Case</em> feature.
    </p>
  </note>
</page>
